/*
@filename: onPlayerRespawn.sqf
Author:
	
	Rodriguez.G

Last modified:

	7/19/2014
	
Description:

	Scripts which are run on death
	
______________________________________________________*/
waitUntil {!isNull player};
player enableFatigue false;	
