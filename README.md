# The 23rd Gaming Division Framework #
A quick drag and drop method of getting the basic essentials required for any 23rd Gaming Division Mission.

### What is this repository for? ###

* Download and Maintain the 23rd Gaming Division Framework

### How do I get set up? ###

* Download the Zip file from this repository
* Create a new folder, or create a new mission in the multiplayer editor
* Overwrite all the data files in the newly created mission file with theses ones
* Reload the editor
* Begin Developing your mission
